# Django Tutorials by Steps

## What is the repository for ?

Know how to use Django with docker tools by belew steps/exercises of tutorial.

## Exercises

**Exercise.6**

1. Customize admin UI.
~~~
#Filename: polls/admin.py
from django.contrib import admin

from .models import Question, Choice

admin.site.register(Question)
admin.site.register(Choice)
~~~

~~~
#Filename: polls/admin.py
from django.contrib import admin

from .models import Choice, Question


# class ChoiceInline(admin.StackedInline):
class ChoiceInline(admin.TabularInline):
    model = Choice
    extra = 3


class QuestionAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,               {'fields': ['question_text']}),
        ('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
    ]
    inlines = [ChoiceInline]
    list_display = ('question_text', 'pub_date', 'was_published_recently')
    list_filter = ['pub_date']
    search_fields = ['question_text']

admin.site.register(Question, QuestionAdmin)
~~~

**Exercise.5**

1. Creates static folder(e.g. polls/static)

> cd polls && mkdir -p static

2. Creates polls/static/polls/style.css

~~~
li a {
    color: green;
}
~~~

3. Edit templates/polls/index.html template.
~~~

{% load static %}

<link rel="stylesheet" type="text/css" href="{% static 'polls/style.css' %}">
...
~~~

4. cd django_container project and build static files.

> cd django_container 

> docker-compose exec django python manage.py collectstatic

5. Open an Web browser to view result.

> http://localhost:8000/polls/

**Exercise.4**

1. Creates vote form into polls/templates/polls/detail.html template.

~~~
<!-- Filename: dj_tutorial_steps/polls/templates/polls/detail.html -->
<h1>{{ question.question_text }}</h1>

{% if error_message %}<p><strong>{{ error_message }}</strong></p>{% endif %}

<!--Setting server side url-->
<form action="{% url 'polls:vote' question.id %}" method="post">
{% csrf_token %}
{% for choice in question.choice_set.all %}
    <input type="radio" name="choice" id="choice{{ forloop.counter }}" value="{{ choice.id }}">
    <label for="choice{{ forloop.counter }}">{{ choice.choice_text }}</label><br>
{% endfor %}
<input type="submit" value="Vote">
</form>

~~~


> Go to http://localhost:8000/polls/ and click hyperlink.

2. Creates vote results templates.

~~~
<!--Filename: dj_tutorial_steps/polls/templates/polls/results.html-->
<h1>{{ question.question_text }}</h1>

<ul>
{% for choice in question.choice_set.all %}
    <li>{{ choice.choice_text }} -- {{ choice.votes }} vote{{ choice.votes|pluralize }}</li>
{% endfor %}
</ul>

<a href="{% url 'polls:detail' question.id %}">Vote again?</a>
~~~

3. Writes vote action and display result in views.py
~~~
# Filename: dj_tutorial_steps/polls/views.py
from django.http import HttpResponseRedirect
from django.urls import reverse

....

def results(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    return render(request, 'polls/results.html', {'question': question})

def vote(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    try:
        selected_choice = question.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        # Redisplay the question voting form.
        return render(request, 'polls/detail.html', {
            'question': question,
            'error_message': "You didn't select a choice.",
        })
    else:
        selected_choice.votes += 1
        selected_choice.save()
        # Always return an HttpResponseRedirect after successfully dealing
        # with POST data. This prevents data from being posted twice if a
        # user hits the Back button.
        # This tip isn’t specific to Django;
        # it’s just good Web development practice.
        return HttpResponseRedirect(reverse('polls:results', args=(question.id,)))

...
~~~

4. Try to vote for questions.

> http://localhost:8000/polls/

5. Django provides a shortcut "generic views": Less code is better

   * Convert the URLconf. 
   * Delete some of the old, unneeded views.
   * Introduce new views based on Django’s generic views.

6. Amend URL.

~~~
# Filename: dj_tutorial_steps/polls/urls.py
from django.urls import path

from . import views

app_name = 'polls'

urlpatterns = [
    # ex: /polls/
    path('', views.IndexView.as_view(), name='index'),
    # ex: /polls/5/
    path('<int:pk>/', views.DetailView.as_view(), name='detail'),
    # ex: /polls/5/results/
    path('<int:pk>/results/', views.ResultsView.as_view(), name='results'),
    # ex: /polls/5/vote/
    path('<int:question_id>/vote/', views.vote, name='vote'),
]
~~~

7. Amend View.
~~~
# Filename: dj_tutorial_steps/polls/views.py

...

from django.views import generic
from .models import Choice, Question

...

class IndexView(generic.ListView):
    template_name = 'polls/index.html'
    context_object_name = 'latest_question_list'

    def get_queryset(self):
        """Return the last five published questions."""
        return Question.objects.order_by('-pub_date')[:5]


class DetailView(generic.DetailView):
    model = Question
    template_name = 'polls/detail.html'


class ResultsView(generic.DetailView):
    model = Question
    template_name = 'polls/results.html'

...

~~~

8. Try to vote again.

> http://localhost:8000/polls/



**Exercise.3**
1. Edit views.py and urls.py to open url entrypoint.

~~~
# Filename: dj_tutorial_steps/polls/views.py

from django.http import HttpResponse
from .models import Question

def index(request):
    latest_question_list = Question.objects.order_by('-pub_date')[:5]
    output = ', '.join([q.question_text for q in latest_question_list])
    return HttpResponse(output)

def detail(request, question_id):
    return HttpResponse("You're looking at question %s." % question_id)

def results(request, question_id):
    response = "You're looking at the results of question %s."
    return HttpResponse(response % question_id)

def vote(request, question_id):
    return HttpResponse("You're voting on question %s." % question_id)

~~~

~~~
# Filename: dj_tutorial_steps/polls/urls.py
from django.urls import path

from . import views

urlpatterns = [
    # ex: /polls/
    path('', views.index, name='index'),
    # ex: /polls/5/
    path('<int:question_id>/', views.detail, name='detail'),
    # ex: /polls/5/results/
    path('<int:question_id>/results/', views.results, name='results'),
    # ex: /polls/5/vote/
    path('<int:question_id>/vote/', views.vote, name='vote'),
]
~~~

2. Open an Web browser and go to below links.

> http://localhost:8000/polls/

> http://localhost:8000/polls/5/

> http://localhost:8000/polls/5/results/

> http://localhost:8000/polls/5/vote/

3. Creates polls/templates/polls folder and create below "index.html" template.

~~~
<!-- Filename: dj_tutorial_steps/polls/templates/polls/index.html -->
{% if latest_question_list %}
    <ul>
    {% for question in latest_question_list %}
        <li><a href="/polls/{{ question.id }}/">{{ question.question_text }}</a></li>
    {% endfor %}
    </ul>
{% else %}
    <p>No polls are available.</p>
{% endif %}
~~~

4. Edit views.py to link above index template to "index" function of views.

> from django.shortcuts import render

> return render(request, 'polls/index.html', context)

~~~
# Filename: dj_tutorial_steps/polls/views.py
from django.http import HttpResponse
from django.shortcuts import render
from .models import Question


def index(request):
    latest_question_list = Question.objects.order_by('-pub_date')[:5]
    context = {'latest_question_list': latest_question_list}
    return render(request, 'polls/index.html', context)
...
~~~

> http://localhost:8000/polls/

5. Raising a 404 error for handling no data condition with Question detail example.

~~~
<!-- Filename: dj_tutorial_steps/polls/templates/polls/detail.html -->
<h1>{{ question.question_text }}</h1>
<ul>
{% for choice in question.choice_set.all %}
    <li>{{ choice.choice_text }}</li>
{% endfor %}
</ul>
~~~

> from django.shortcuts import get_object_or_404, render

~~~
# Filename: dj_tutorial_steps/polls/views.py
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, render
from django.shortcuts import render

from .models import Question

...

def detail(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    return render(request, 'polls/detail.html', {'question': question})
...

~~~


6. Removing hardcoded URLs in templates(e.g. templates/polls/index.html).

Original url format:

> href="/polls/{{ question.id }}/"

Django URL name format:

> href="{% url 'detail' question.id %}"

Django App URL name format:

> app_name = 'polls'
~~~
# Filename: dj_tutorial_steps/polls/urls.py
from . import views
....
app_name = 'polls'
urlpatterns = [
    ...
]
~~~

> href="{% url 'polls:detail' question.id %}"

View modified result:

> http://localhost:8000/polls/



**Exercise.2**


1. Login PostgreSQL sever of docker container and create django_tutorial_steps table.
2. Edit django-tutorials-app-by-steps/dj_tutorial_steps/settings.py

~~~
# Filename: dj_tutorial_steps/settings.py

...

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'django_tutorial_steps',
        'USER': 'postgres',
        'PASSWORD': 'postgres',
        'HOST': 'postgres',
        'PORT': '5432',
    }
}

...
~~~

3. You may change TIME_ZONE for your country.

4. Write models.py for Question and Choice tables and its columns.
~~~
# Filename: dj_tutorial_steps/polls/modles.py
from django.db import models


class Question(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')


class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

~~~

5. Adds polls into INSTALLED_APPS variable.

~~~
# Filename: dj_tutorial_steps/setttings.py

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'polls.apps.PollsConfig'
]
~~~

6. Run "docker-compose exec django python manage.py makemigrations" to creates tables and colums for models.py on django-container folder.

> docker-compose exec django python manage.py makemigrations

~~~
Starting djangocontainer_postgres_1 ... done
Migrations for 'polls':
  polls/migrations/0001_initial.py
    - Create model Question
    - Create model Choice
~~~

7. Run "docker-compose exec django python manage.py migrate".

> docker-compose exec django python manage.py migrate

~~~
Starting djangocontainer_postgres_1 ... done
Creating djangocontainer_makemigrations_1 ... done
Operations to perform:
  Apply all migrations: admin, auth, contenttypes, polls, sessions
Running migrations:
  Applying polls.0001_initial... OK
~~~

8. How to know Django executes SQL statements.

> docker-compose exec django python manage.py sqlmigrate polls 0001

~~~
BEGIN;
--
-- Create model Question
--
CREATE TABLE "polls_question" ("id" serial NOT NULL PRIMARY KEY, "question_text" varchar(200) NOT NULL, "pub_date" timestamp with time zone NOT NULL);
--
-- Create model Choice
--
CREATE TABLE "polls_choice" ("id" serial NOT NULL PRIMARY KEY, "choice_text" varchar(200) NOT NULL, "votes" integer NOT NULL, "question_id" integer NOT NULL);
ALTER TABLE "polls_choice" ADD CONSTRAINT "polls_choice_question_id_c5b4b260_fk_polls_question_id" FOREIGN KEY ("question_id") REFERENCES "polls_question" ("id") DEFERRABLE INITIALLY DEFERRED;
CREATE INDEX "polls_choice_question_id_c5b4b260" ON "polls_choice" ("question_id");
COMMIT;
~~~

9. Uses interactive Python Shell to practice Django models.

> docker-compose exec django python manage.py shell

~~~
Python 3.6.8 (default, May  8 2019, 05:35:00) 
[GCC 6.3.0 20170516] on linux
Type "help", "copyright", "credits" or "license" for more information.
(InteractiveConsole)
>>> 
~~~

10. Django Models CRUD (Create/Read/Update/Delete)

Create and read models.
~~~
Python 3.6.8 (default, May  8 2019, 05:35:00) 
[GCC 6.3.0 20170516] on linux
Type "help", "copyright", "credits" or "license" for more information.
(InteractiveConsole)
>>> from polls.models import Choice, Question
>>> Question.objects.all()
<QuerySet []>
>>> from django.utils import timezone
>>> q = Question(question_text="What's new?", pub_date=timezone.now())
>>> q.save()
>>> q.id
1
>>> q.question_text
"What's new?"
>>> q.pub_date
datetime.datetime(2019, 5, 14, 8, 16, 27, 574309, tzinfo=<UTC>)
>>> q.question_text="What's up? Willis!"
>>> q.save()
>>> Question.objects.all()
<QuerySet [<Question: Question object (1)>]>
>>> q.question_text
"What's up? Willis!"
>>> list = Question.objects.all()
>>> list[0].question_text
"What's up? Willis!"
~~~

12. Do you confuse "<QuerySet [<Question: Question object (1)>]>"?

~~~
Python 3.6.8 (default, May  8 2019, 05:35:00) 
[GCC 6.3.0 20170516] on linux
Type "help", "copyright", "credits" or "license" for more information.
(InteractiveConsole)
>>> from polls.models import Choice, Question
>>> Question.objects.all()
<QuerySet [<Question: What's up? Willis!>]>
>>> Question.objects.filter(id=1)
<QuerySet [<Question: What's up? Willis!>]>
>>> Question.objects.filter(question_text__startswith='What')
<QuerySet [<Question: What's up? Willis!>]>
>>> from django.utils import timezone
>>> current_year = timezone.now().year
>>> Question.objects.get(pub_date__year=current_year)
<Question: What's up? Willis!>
>>> Question.objects.get(id=2)
Traceback (most recent call last):
  File "<console>", line 1, in <module>
  File "/usr/local/lib/python3.6/site-packages/django/db/models/manager.py", line 82, in manager_method
    return getattr(self.get_queryset(), name)(*args, **kwargs)
  File "/usr/local/lib/python3.6/site-packages/django/db/models/query.py", line 408, in get
    self.model._meta.object_name
polls.models.Question.DoesNotExist: Question matching query does not exist.
>>> Question.objects.get(pk=1)
<Question: What's up? Willis!>
>>> q = Question.objects.get(pk=1)
>>> q.was_published_recently()
~~~

13. Adds choice in specific queuestion.

~~~
>>> q = Question.objects.get(pk=1)
>>> q.choice_set.all()
<QuerySet []>
>>> q.choice_set.create(choice_text='Not much', votes=0)
<Choice: Not much>
>>> q.choice_set.create(choice_text='The sky', votes=0)
<Choice: The sky>
>>> c = q.choice_set.create(choice_text='Just hacking again', votes=0)
>>> c.question
<Question: What's up? Willis!>
>>> q.choice_set.all()
<QuerySet [<Choice: Just hacking again>, <Choice: The sky>, <Choice: Not much>]>
>>> q.choice_set.count()
3
~~~
14. Django model deletion

Delete choice model object.

~~~
>>> from django.utils import timezone
>>> current_year = timezone.now().year
>>> Choice.objects.filter(question__pub_date__year=current_year)
<QuerySet [<Choice: Just hacking again>, <Choice: The sky>, <Choice: Not much>]>
>>> c = q.choice_set.filter(choice_text__startswith='Just hacking')
>>> q.choice_set.filter(choice_text__startswith='Just hacking')
<QuerySet [<Choice: Just hacking again>]>
>>> c = q.choice_set.filter(choice_text__startswith='Just hacking')
>>> c.delete()
(1, {'polls.Choice': 1})
>>> q.choice_set.filter(choice_text__startswith='Just hacking')<QuerySet []>
>>> q.choice_set.count()
2
~~~

15. Creating an admin user.

> docker-compose exec django python manage.py createsuperuser

~~~
Username (leave blank to use 'root'): willis
Email address: misweyu2007@gmail.com
Password: 
Password (again): 
The password is too similar to the username.
Bypass password validation and create user anyway? [y/N]: N
Password: 
Password (again): 
Superuser created successfully.
~~~

16. Open a Web browser and got to http://localhost:8000/admin.

17. Make the poll app modifiable in the admin and check on django admin dashboard.

~~~
# Filename: dj_tutorial_steps/polls/admin.py

from django.contrib import admin

from .models import Question

admin.site.register(Question)
~~~

**Exercise.1**

1. Go to Django Container

> ll {Project folder}/django-container
~~~
total 36
drwxr-xr-x  4 root   root   4096  五  12 14:13 ./
drwxrwxr-x  6 willis willis 4096  五  12 14:33 ../
-rw-r--r--  1 root   root    880  五  12 14:38 docker-compose.yml
-rw-r--r--  1 root   root    235  五  11 22:57 Dockerfile
-rw-r--r--  1 root   root    265  五  12 14:39 .env
drwxr-xr-x  8 root   root   4096  五  12 14:24 .git/
drwx------ 19     70 root   4096  五  12 14:58 pgdata-godslab/
-rw-r--r--  1 root   root   1881  五  12 14:18 README.md
-rw-r--r--  1 root   root     35  五  11 21:30 requirements.txt
~~~

2. Check whether django exists docker container.

> docker-compose exec django sh
 
> python -m django --version
~~~
2.2.1
~~~

> git clone https://gitlab.com/topic-tutorials/django-tutorials-app-by-steps.git

> ls {Project folder}/django-tutorials-app-by-steps

3. Set DJANGO_PROJECT_DIR={Project folder}/django-tutorials-app-by-steps in {Project folder}/django-container/.env file.

> sudo docker-compose run django django-admin startproject dj_tutorial_steps .

> ll {Project folder}/django-tutorials-app-by-steps

~~~
total 140
-rw-r--r-- 1 root root 131072  五  12 14:41 db.sqlite3
drwxr-xr-x 3 root root   4096  五  12 14:41 dj_tutorial_steps
-rwxr-xr-x 1 root root    637  五  12 14:41 manage.py
-rw-r--r-- 1 root root    144  五  12 14:51 README.md
~~~

> cd {Project folder}/django-container && docker-compose up

~~~
Recreating djangocontainer_postgres_1 ... done
Recreating djangocontainer_migration_1 ... done
Recreating djangocontainer_django_1    ... done
Attaching to djangocontainer_postgres_1, djangocontainer_migration_1, djangocontainer_django_1
postgres_1   | 2019-05-12 07:08:40.904 UTC [1] LOG:  listening on IPv4 address "0.0.0.0", port 5432
postgres_1   | 2019-05-12 07:08:40.904 UTC [1] LOG:  listening on IPv6 address "::", port 5432
postgres_1   | 2019-05-12 07:08:40.929 UTC [1] LOG:  listening on Unix socket "/var/run/postgresql/.s.PGSQL.5432"
postgres_1   | 2019-05-12 07:08:40.965 UTC [18] LOG:  database system was shut down at 2019-05-12 07:08:37 UTC
postgres_1   | 2019-05-12 07:08:40.974 UTC [1] LOG:  database system is ready to accept connections
migration_1  | Operations to perform:
migration_1  |   Apply all migrations: admin, auth, contenttypes, sessions
migration_1  | Running migrations:
migration_1  |   No migrations to apply.
djangocontainer_migration_1 exited with code 0
django_1     | Watching for file changes with StatReloader
~~~

4. Open another terminal to find your IP (website address).

> ifconfig

~~~
wlp2s0    Link encap:Ethernet  HWaddr a**:**:**:**:**:**  
          inet addr:192.168.2.191  Bcast:192.168.2.255  Mask:255.255.255.0
          inet6 addr: fe80::9579:60ed:8399:2e08/64 Scope:Link
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:655773 errors:0 dropped:1 overruns:0 frame:0
          TX packets:425001 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000 
          RX bytes:914057367 (914.0 MB)  TX bytes:105724845 (105.7 MB)
# Open another terminal in Windows OS.
~~~

5. Creates poll app.
> docker-compose exec django python manage.py startapp polls